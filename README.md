# qt-tech-doc

#### 介绍
钱拓技术文档展示网页。在Thymeleaf中使用Editor.md组件将markdown文件在线转成html页面展示，可以动态新增文章在线展示。

#### 软件架构
spring boot项目，页面模板引擎使用Thymeleaf，前端框架bootstrap。


#### 安装教程

1.  进入到项目根目录
2.  运行Maven打包命令  mvn clean package -Dmaven.test.skip=true
3.  进入到target/build 目录下，启停脚本startup.sh,shutdown.sh

#### 使用说明

1.  新增加文章时，需要在build/resources/templates/common/siderbar.html 页面中添加新文章导航地址
![输入图片说明](images/image01.png)
2.  比如新增加的文章名是kubernets helm安装部署，对应的markdown文件是k8s03.md，那么在上图标注的 kubernets集群部署 下新增加一个li标签，如：`<li class="nav-item"> <a class="nav-link" href="../pages/ui-features/typography.html" th:href="@{'/doc?filename=k8s03.md'}">kubernets helm安装部署</a></li>`
3. 修改好siderbar.html页面后保存，将新增的k8s03.md markdown文件放至到build/resources/markdown目录下
4. 刷新页面查看页面效果
5. 如果要新建文章分类，可以复制上图中Spring文章的html节点进行修改，剩下的新增文章步骤参考步骤2，步骤3。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
