(function() {
  "use strict"; // Start of use strict

  // Show the navbar when the page is scrolled up
  var MQL = 992;
  var vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
  var mainNav = document.querySelector('#mainNav');
  mainNav.classList.add('is-visible', 'is-fixed');
    
  //primary navigation slide-in effect
  if (mainNav && vw > MQL) {
    var headerHeight = mainNav.offsetHeight;
    var previousTop = window.pageYOffset;
    
    window.addEventListener('scroll', function() {
      var currentTop = window.pageYOffset;
      //check if user is scrolling up
      if (currentTop < previousTop) {
          //if scrolling up...
          mainNav.classList.add('is-visible', 'is-fixed');
      } else if (currentTop > previousTop) {
          //if scrolling down...
          // console.log("currentTop:" + currentTop);
          if (currentTop > 0) {
              mainNav.classList.remove('is-visible', 'is-fixed');
          }
          
      }
        previousTop = currentTop;
    });
  }

})(); // End of use strict
