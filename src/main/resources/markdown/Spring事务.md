作者：刘康
时间：2022-09-01
## 事务

### 概念：

> 指的是事务是逻辑上一组操作，组成这组操作的各个逻辑单元，要么一起成功，要么一起失败。

### 基本特征

```markdown
A：Atomic，原子性，将所有SQL作为原子工作单元执行，要么全部执行，要么全部不执行；有一个执行失败则修改操作全部回滚
C：Consistent，一致性，事务完成后，所有数据的状态都是一致的，即A账户只要减去了100，B账户则必定加上了100；
I：Isolation，隔离性，如果有多个事务并发执行，每个事务作出的修改必须与其他事务隔离；
D：Duration，持久性，即事务完成后，对数据库数据的修改被持久化存储。
```

### 数据库事务:基于innodb引擎

两种类型：隐式事务、显式事务

```
对于单条SQL语句，数据库系统自动将其作为一个事务执行，这种事务被称为隐式事务。
多条SQL语句作为一个事务执行，使用BEGIN开启一个事务，使用COMMIT提交一个事务，这种事务被称为显式事务
```

```
BEGIN;
UPDATE accounts SET balance = balance - 100 WHERE id = 1;
UPDATE accounts SET balance = balance + 100 WHERE id = 2;
COMMIT;
【或者回滚使用ROLLBACK;】
```

### JDBC实现事务伪代码

```java
// 1.获取数据库连接对象
Connection conn = openConnection();
try {
// 2.关闭自动提交
    conn.setAutoCommit(false);
// 3.执行多条SQL语句
    insert(); 
    update(); 
    delete();
// 4.提交事务
    conn.commit();
} catch (SQLException e) {
// 5.遇到异常进行回滚事务
    conn.rollback();
} finally {
// 关闭资源
    conn.close();
}
```

```
开启事务的关键代码是conn.setAutoCommit(false)，表示关闭自动提交。提交事务的代码在执行完指定的若干条SQL语句后，调用conn.commit()。要注意事务不是总能成功，如果事务提交失败，会抛出SQL异常（也可能在执行SQL语句的时候就抛出了），此时我们必须捕获并调用conn.rollback()回滚事务。最后，在finally中通过conn.setAutoCommit(true)把Connection对象的状态恢复到初始值。
```

## Spring事务

Spring事务是基于SpringAOP实现的，但本质上还是和JDBC类似是操作数据库事务实现的。

- 声明式事务、编程式事务

> 编程式程式事务会侵入到了业务代码里面，但是提供了更加详细的事务管理，而且避免由于spring aop问题，导致事务失效的问题【业务复杂推荐使用】
>
> 而声明式事务由于基于 AOP，所以既能起到事务管理的作用，又可以不影响业务代码的具体实现，但只能控制到方法级别。【业务简单推荐使用】

- 声明式事务

  ```
  @Transactional(rollbackFor = Throwable.class)
  ```

  ==思考一下为什么要指定rollbackFor？又要指定Exception或Throwable？==

- 编程式事务实现

  ```java
  @Autowired
  private TransactionTemplate transactionTemplate;
  
  @Test
  void test09() {
  
      // 1.有返回值【TransactionCallback】
      Boolean resultFlag = this.transactionTemplate.execute(transactionStatus -> {
          UserInfo userInfo = new UserInfo();
          userInfo.setUserId(1);
          userInfo.setOpenId("xxxxx1");
          this.userInfoMapper.updateById(userInfo);
  
          UserInfo userInfo02 = new UserInfo();
          userInfo02.setUserId(1);
          userInfo02.setNickName("派对阿布");
  
          this.userInfoMapper.updateById(userInfo02);
  
          // throw new RuntimeException();
  
          return Boolean.TRUE;
      });
  
      // 2.没有返回值【executeWithoutResult】
      this.transactionTemplate.executeWithoutResult(transactionStatus -> {
  
          UserInfo userInfo = new UserInfo();
          userInfo.setUserId(1);
          userInfo.setOpenId("xxxxx1");
          this.userInfoMapper.updateById(userInfo);
  
          UserInfo userInfo02 = new UserInfo();
          userInfo02.setUserId(1);
          userInfo02.setNickName("派对阿布");
  
          this.userInfoMapper.updateById(userInfo02);
      });
  }
  ```

  ==声明式事务那么好用，为什么还提供编程式事务？什么时候使用编程式事务呢？==

### 原理

```markdown
JDK动态代理：通过`反射`生成实现代理对象接口的匿名类，通过生成`代理实例`时传递的InvocationHandler处理程序实现方法增强。

CGLIB动态代理：基于ASM，`操作字节码`，通过加载代理对象的类字节码，为代理对象创建一个子类，并在子类中拦截父类方法并织入方法增强逻辑。
```

两者性能比较：JDK > CGLIB，这也就是Spring默认的动态代理方式为JDK

比如Mybatis生成Mapper代理对象就是基于JDK动态代理

![image-20220731150639815](/images/image-20220731150639815.png)

## Spring事务配置

### 传播行为

```markdown
propagation_required 		-- 支持当前事务，如果有事务就使用同一个事务，如果当前没有事务，就新建一个事务。`这是最常见的`

propagation_supports 		-- 表示当前方法不需要事务上下文，但是如果存在当前事务的话，那么该方法会在这个事务中运行

propagation_mandatory 		-- 表示该方法必须在事务中运行，如果当前事务不存在，则会抛出一个异常

propagation_requires_new 	-- 表示当前方法必须运行在它自己的事务中。一个新的事务将被启动。如果存在当前事务，在该方法执行期间，当前事务会被挂起

propagation_not_supported 	-- 以非事务方式执行操作，如果当前存在事务，就把当前事务挂起，不使用这个事务。

propagation_never 			-- 以非事务方式执行，如果当前存在事务，则抛出异常。

propagation_nested 			-- 如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则进行与
propagation_required类似的操作。
```

> propagation_nested理解
>
> - 依赖于JDBC3.0提供的SavePoint技术
>
> - 删除用户、删除订单、删除相关用户报表逻辑。
>
>   在删除用户后，设置savePoint，执行删除订单。此时删除用户、删除订单在同一个事务中；
>
>   执行删除相关用户报表，设置savePoint。删除订单、删除相关用户报表在同一个事务中；
>
>   如果删除相关用户报表逻辑出现异常，则回滚上一个savePoint点【删除订单】，如果删除订单逻辑出现异常，则回滚上一个savePoint点【删除用户】。每次只能回滚上一个savePoint。

这七种事务传播机制最常用的就两种：

REQUIRED：一个事务，要么成功，要么失败

REQUIRES_NEW：两个不同事务，彼此之间没有关系。一个事务失败了不影响另一个事务

#### 伪代码

传播行为伪代码模拟：有a,b,c,d,e等5个方法，a中调用b,c,d,e方法的传播行为在小括号中标出

```java
a(required){
    .....a
	b(required);
	c(requires_new){
        ....
    };
	d(required);
	e(requires_new){
        
    };
	// a方法的业务逻辑
    。。。。a
}
```

问题：

1. a方法的业务出现异常，会怎样？a,b,d回滚 c,e不回滚
2. d方法出现异常，会怎样？a,b,d回滚 c,e不回滚
3. e方法出现异常，会怎样？a,b,d,e回滚 c不回滚，e方法出异常会上抛影响到上级方法
4. b方法出现异常，会怎样？a,b回滚 c,d,e未执行

加点难度：

```java
a(required){
	b(required){
		f(requires_new);
		g(required)
	}
	c(requires_new){
		h(requires_new)
		i(required)				// i使用c的事务
        // c业务逻辑....
	}
	d(required);
	e(requires_new);
	// a方法的业务逻辑....
}
```

问题：

1. a方法业务出现异常【a、b、g、d回滚；其他都不回滚】
2. e方法抛出异常【e，a、b、g、d回滚；其他都不回滚】
3. d方法抛出异常【a、b、g、d回滚；其他都不回滚】
4. i方法抛出异常【i、c、a、b、g回滚；d、e执行不到；其他都不回滚】
5. h方法抛出异常【h、c、a、b、g回滚；i执行不到；其他都不回滚】
6. c业务逻辑出现异常【c、i、a、b、g回滚；d、e执行不到；其他都不回滚】

### 隔离级别

事务的第二个维度就是隔离级别（isolation level）。隔离级别定义了一个事务可能受其他并发事务影响的程度。**默认使用的是使用底层数据存储的默认隔离级别**

- 并发事务引起的问题

在典型的应用程序中，多个事务并发运行，经常会操作相同的数据来完成各自的任务。并发虽然是必须的，但可能会导致一下的问题。

> 1.脏读
>
> ——脏读发生在一个事务读取了另一个事务改写但尚未提交的数据时。如果改写在稍后被回滚了，那么第一个事务获取的数据就是无效的。
>
> 2.不可重复读
>
> ——不可重复读发生在一个事务执行相同的查询两次或两次以上，但是每次都得到不同的数据时。这通常是因为另一个并发事务在两次查询期间进行了更新。
>
> 3.幻读
>
> ——幻读与不可重复读类似。它发生在一个事务（T1）读取了几行数据，接着另一个并发事务（T2）插入了一些数据时。在随后的查询中，第一个事务（T1）就会发现多了一些原本不存在的记录。

****

解决以上问题的四种隔离级别

```
隔离级别从低到高如下：【隔离级别越高，性能越差】
读未提交、读已提交、可重复读、可序列化

读未提交：存在脏读问题
读已提交：解决了脏读问题，存在不可重复读
可重复读：解决了账户、不可重复读问题，存在幻读问题
可串行化：解决了以上三种问题，但是性能最差
```

MySql中Innodb默认隔离级别是可重复读、Oracle默认隔离级别是读已提交。所以Oracle要比MySql性能更好一些

****

**不可重复读与幻读的区别**

不可重复读的重点是修改:同样的条件, 你读取过的数据, 再次读取出来发现值不一样了例如：在事务 1 中，Mary 读取了自己的工资为 1000,操作并没有完成

```
con1 = getConnection();    select salary from employee empId ="Mary";
```

在事务 2 中，这时财务人员修改了 Mary 的工资为 2000,并提交了事务.

```
con2 = getConnection();    update employee set salary = 2000;    con2.commit();
```

在事务 1 中，Mary 再次读取自己的工资时，工资变为了 2000

```
con1    select salary from employee empId ="Mary";
```

在一个事务中前后两次读取的结果并不一致，导致了不可重复读。

****

幻读的重点在于新增或者删除：同样的条件, 第 1 次和第 2 次读出来的记录数不一样例如：目前工资为 1000 的员工有 10 人。事务 1,读取所有工资为 1000 的员工。

```
con1 = getConnection();    Select * from employee where salary =1000;
```

共读取 10 条记录

这时另一个事务2向 employee 表插入了一条员工记录，工资也为 1000

```
con2 = getConnection();    Insert into employee(empId,salary) values("Lili",1000);    con2.commit();
```

事务 1 再次读取所有工资为 1000 的员工

```
//con1    select * from employee where salary =1000;
```

共读取到了 11 条记录，这就产生了幻像读。

> 从总的结果来看, 似乎不可重复读和幻读都表现为两次读取的结果不一致。但如果你从控制的角度来看, 两者的区别就比较大。

### 只读

事务的第三个特性是它是否为只读事务。如果事务只对后端的数据库进行该操作，**数据库可以利用事务的只读特性来进行一些特定的优化。**通过将事务设置为只读，你就可以给数据库一个机会，让它应用它认为合适的优化措施。

### 事务超时

为了使应用程序很好地运行，事务不能运行太长的时间。因为事务可能涉及对后端数据库的锁定，所以长时间的事务会不必要的占用数据库连接资源。事务超时就是事务的一个定时器，**在特定时间内事务如果没有执行完毕，那么就会自动回滚，而不是一直等待其结束。**

```
此事务的超时时间（以秒为单位）。
默认为底层事务系统的默认超时。
超时时间是从当前事务第一个sql开始到最后一个sql执行结束的中间时间
```

### 回滚规则

默认情况下，**事务只有遇到运行期异常时才会回滚，而在遇到检查型异常时不会回滚**（这一行为与 EJB 的回滚行为是一致的）但是你可以声明事务在遇到特定的检查型异常时像遇到运行期异常那样回滚。同样，你还可以声明事务遇到特定的异常不回滚，即使这些异常是运行期异常。

![image-20220731155834395](/images/image-20220731155834395.png)

**==这个也就是为什么使用声明式事务时为什么要指定rollbackFor，它默认只对RuntimeException和Error进行回滚，要想对所有异常情况进行回滚，那么就是要指定Throwable.class==**

<img src="/images/image-20220731160334093.png" alt="image-20220731160334093" style="zoom: 50%;" />

## Spring声明式事务失效几种情况

- 非public方法

  <img src="/images/image-20220731194746994.png" alt="image-20220731194746994" style="zoom: 33%;" />

  为什么必须是public呢？

  > spring事务底层默认使用了jdk动态代理，动态代理对象再调用增强逻辑，我们使用的自定义类，spring是不知道能不能访问，为了能正常访问，需要是public。

- 异常没被抛出，被catch处理掉了

  > spring事务通过捕获抛出的异常来处理事务回滚，要不然rollbackFor要它何用？

*****



1. 两个方法增加的数据会不会回滚？

2. 方法B事务注解是否起作用？

   

- 使用 `@Transactional`同一个类中方法之间调用

  ```java
  class TestService{
  	
      @Transactional(rollbackFor=AException.class)
      methodA{
           // insert into 
          TestService t = (TestService) AopContext.currentProxy();
          t.methodB();
      }
      
      @Transactional(rollbackFor=BException.class)
      methodB{
          // insert into 
          
          throw new RuntimeException();
          
      };
  }
  ```

  ```
  1. 方法A加@Transactional，方法B加@Transactional，方法A调用方法B，方法B的事务会不会起作用，遇到异常时可以回滚吗？
  
     答案：方法A、方法B公用一个方法A的事务，但方法B加上的@Transactional不起作用，
     原因：spring的事务是通过数据库连接来实现的,当前线程中保存了一个map，key是数据源，value是数据库连接对象。当spring遇到该注解时，会自动从数据库连接池中获取connection，并开启事务将数据源做为key，数据库连接对象作为value，封装成一个map，然后绑定到当前线程的ThreadLocal中上。
     上述代码逻辑相当于将B方法逻辑嵌入到方法A中，在执行B方法时是同一个线程内调用的，从ThreadLocal中获取的Connection对象是与方法A同一个Connection对象，自然是可以执行事务操作的，只不过两个方法使用的是同一个事务罢了。
  
  2. 方法A加@Transactional，方法B不加@Transactional，方法A调用方法B，方法B的事务会不会起作用？遇到异常时可以回滚吗？
  
     答案：同上
  
  3. 方法A不加@Transactional，方法B加@Transactional，方法A调用方法B，方法B的事务会不会起作用？
     答案：方法A不受事务管理、自然不会与受事务管理使用同一个事务了
  ```

  怎么解决第一种都添加事务注解，但是不生效的问题呢？

  > 1. 在该Service类中注入自己
  >
  >    ```java
  >    @Service
  >    public class UserServiceImpl implements UserService {
  >    
  >        @Autowired
  >        private UserService userService;
  >    }
  >    ```
  >
  >    > 可能有些人可能会有这样的疑问：这种做法会不会出现循环依赖问题？
  >    >
  >    > 答案：不会。spring ioc内部的三级缓存保证了它，不会出现循环依赖问题
  >
  > 2. 使用AopContext获取上下文代理对象
  >
  >    ```java
  >    // 开启暴露代理对象，默认是关闭的
  >    @EnableAspectJAutoProxy(exposeProxy = true)
  >    // 方法中获取代理对象
  >    T t = (T) AopContext.currentProxy();
  >    ```

- 数据库本身不支持事务，比如MyISAM底层不支持事务，innodb支持事务

- rollbackFor属性设置错误

- <u>方法被final或static修饰也不能</u>

  ```
  final或static修饰的方法失效，spring事务底层使用了aop动态代理，用final修饰了，那么在它的代理类中，就无法重写该方法；
  static的，同样无法通过动态代理，变成事务方法。（static不能被动态代理，因为不能被重写且不属于对象）
  ```

- 多线程调用也不行

  > spring的事务是通过数据库连接来实现的。当前线程中保存了一个map，key是数据源，value是数据库连接，只有拥有同一个数据库连接对象才能同时提交和回滚。如果在不同的线程，拿到的数据库连接对象肯定是不一样的，所以是不同的事务

## 长事务

```
有很多人心里已经将Spring事务与`@Transactional`划上了等号，只要有数据库相关操作就直接给方法加上`@Transactional`注解。
即使一个方法中有4、5个查询，只有一个更新逻辑，也同样加了@Transactional进行控制事务
```

### 概念：

```
字面上看就是执行时间过长，实质上来说是占用数据库连接对象时间过长，长时间未提交的事务，当请求过多时，导致数据连接池中的连接对象资源耗尽。
```

### 根本原因：

```
对事务方法进行拆分，尽量让事务变小，变快，减小事务的颗粒度。
```

### 如何解决

```markdown
使用声明式事务的优点很明显，就是使用很简单，可以自动帮我们进行事务的开启、提交以及回滚等操作。使用这种方式，程序员只需要关注业务逻辑就可以了。但有一个最大的缺点，就是事务的颗粒度是整个方法，无法进行精细化控制。
1. 与声明式事务对应的就是编程式事务，使用编程式事务最大的好处就是可以精细化控制事务范围。
2. 如果非得要使用@Transactional处理事务，那么将需要事务管理的方法进行单独抽离成一个事务方法
```

TransactionTemplate两种方式，有返回值和无返回值的事务

```java

@Test
void test09() {

    // 1.有返回值【TransactionCallback】
    Boolean resultFlag = this.transactionTemplate.execute(transactionStatus -> {
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(1);
        userInfo.setOpenId("xxxxx1");
        this.userInfoMapper.updateById(userInfo);

        UserInfo userInfo02 = new UserInfo();
        userInfo02.setUserId(1);
        userInfo02.setNickName("派对阿布");

        this.userInfoMapper.updateById(userInfo02);

        throw new RuntimeException();

        // return Boolean.TRUE;
    });

    // 2.没有返回值【executeWithoutResult】
    this.transactionTemplate.executeWithoutResult(transactionStatus -> {

        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(1);
        userInfo.setOpenId("xxxxx1");
        this.userInfoMapper.updateById(userInfo);

        UserInfo userInfo02 = new UserInfo();
        userInfo02.setUserId(1);
        userInfo02.setNickName("派对阿布");

        this.userInfoMapper.updateById(userInfo02);
    });
}
```

