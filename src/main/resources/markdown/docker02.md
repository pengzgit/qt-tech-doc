## Dockerfile
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654672929458-026ee278-eb37-480a-af9a-dc5ab7d26d85.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=448&id=udf023b29&margin=%5Bobject%20Object%5D&name=image.png&originHeight=896&originWidth=1122&originalType=binary&ratio=1&rotation=0&showTitle=false&size=843491&status=done&style=none&taskId=uc968bc46-2d5e-4773-b86e-59dc473fba8&title=&width=561)
Dockerfile是用来构建Docker镜像的文本文件，是由一条条构建镜像所需的指令和参数构成的脚本。
构建三步骤：

1. 编写Dockerfile文件
1. docker build命令构建镜像
1. docker run依镜像运行容器实例
### Dockerfile 内容基础知识

1. 每条保留字指令都必须为大写字母且后面要跟随至少一个参数
1. 指令按照从上到下，顺序执行
1. #表示注释
1. 每条指令都会创建一个新的镜像层并对镜像进行提交
1. Dockerfile中第一个非注释行必须是FROM指令，用来指定制作当前镜像依据的是哪个基础镜像。
1. Dockerfile中需要调用的文件必须跟Dockerfile文件在同一目录下，或者在其子目录下，父目录或者其它路径无效。
### Dockerfile 指令
> [Dockerfile指令](https://www.cnblogs.com/ccbloom/p/11174186.html)

案例： 
[https://www.cnblogs.com/ZMargo/articles/16277291.html](https://www.cnblogs.com/ZMargo/articles/16277291.html)
[tomcat官方Dockerfile](https://github.com/docker-library/docs/blob/master/tomcat/README.md#supported-tags-and-respective-dockerfile-links)
```dockerfile
docker build -t margo/mypro:v1 .
# -f指定Dockerfile文件的路径
# -t指定镜像名字和TAG
# .指当前目录，这里实际上需要一个上下文路径
```
### 注意点
#### RUN和CMD、ENTRYPOINT区别

- RUN命令执行命令并创建新的镜像层，通常用于安装软件包
- CMD、ENTRYPOINT是设置容器启动后默认执行的命令及其参数
#### CMD和ENTRYPOINT区别

- CMD设置的命令能够被docker run命令后面的命令行参数替换
- CMD命令是用于默认执行的，且如果写了多条CMD命令，则只会执行最后一条，如果后续存在ENTRYPOINT命令，则CMD命令或被充当参数或者覆盖
- ENTRYPOINT配置容器启动时的执行命令（不会被忽略，一定会被执行，即使运行 docker run时指定了其他命令）
- ENTRYPOINT配置的命令如果想被替换，则需要在docker run 后加上参数：--entrypoint=xxx
- Dockerfile中可以存在多个ENTRYPOINT指令，但是只有最后一个会生效
- Dockerfile中如果既有CMD又有ENTRYPOINT，那么最后一条ENTRYPOINT肯定会执行，并且把最后一个CMD已参数形式加入到ENTRYPOINT执行;如果docker run中带有参数，那么此带入的参数将会以参数形式加入到ENTRYPOINT中
```dockerfile
FROM alpine:3.14
RUN apk add --no-cache curl
CMD curl www.baidu.com
```
docker build -t margo/myalpine .
docker run  --name test_alpine margo/myalpine
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654675283198-1bfcbf9d-3a24-4514-9f05-a9f07e739f32.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=352&id=u29357b53&margin=%5Bobject%20Object%5D&name=image.png&originHeight=704&originWidth=1698&originalType=binary&ratio=1&rotation=0&showTitle=false&size=275202&status=done&style=none&taskId=u9df9daee-118a-4dc5-94e9-8fc4c1e3389&title=&width=849)
docker run  --name test_alpine margo/myalpine echo 1234
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654675330797-74264cba-7636-43ec-a464-1f43b047e690.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=37&id=u21810d89&margin=%5Bobject%20Object%5D&name=image.png&originHeight=74&originWidth=1568&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19604&status=done&style=none&taskId=ua48ce734-d917-4b71-a633-0d4f242f60d&title=&width=784)

---

```dockerfile
FROM alpine:3.14
RUN apk add --no-cache curl
CMD curl www.baidu.com
ENTRYPOINT [“echo”, "AABB"]
```
docker build -t margo/myalpine  .
docker run --name t_alpine margo/myalpine
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654676085077-47c7472c-aa5d-450b-8e82-872c8b2b03e7.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=51&id=ub5c509c9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=102&originWidth=1332&originalType=binary&ratio=1&rotation=0&showTitle=false&size=31275&status=done&style=none&taskId=u372161bb-c4ee-495a-a50b-4012b5d8859&title=&width=666)

---

```dockerfile
FROM alpine:3.14
RUN apk add --no-cache curl
CMD curl www.baidu.com
CMD ["echo", "cmd2"]
ENTRYPOINT ["echo", "AABB"]
```
docker build -t margo/myalpine  .
docker run --name t_alpine margo/myalpine
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654676396459-03cfd87a-fca7-4d8d-91f5-b030bc8d6560.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=37&id=u7cbfda9a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=74&originWidth=1374&originalType=binary&ratio=1&rotation=0&showTitle=false&size=19757&status=done&style=none&taskId=uab7e90b8-2f94-405f-ae33-a67738c949d&title=&width=687)

---

```dockerfile
FROM alpine:3.14
RUN apk add --no-cache curl
CMD curl www.baidu.com
CMD ["echo", "cmd2"]
ENTRYPOINT ["echo", "AABB"]
CMD ["cmd", "endddd"]
```
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654676955398-14844482-9e8a-4087-b3e8-c2aae5d6af6b.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=30&id=u6aca144c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=60&originWidth=1300&originalType=binary&ratio=1&rotation=0&showTitle=false&size=16418&status=done&style=none&taskId=uf5ba1cc8-e4fb-46fc-b9a5-e4e9a1ca9ee&title=&width=650)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654676965435-32688bdb-b00a-4006-a7eb-069a276469c9.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=30&id=ue6176d3f&margin=%5Bobject%20Object%5D&name=image.png&originHeight=60&originWidth=1406&originalType=binary&ratio=1&rotation=0&showTitle=false&size=18201&status=done&style=none&taskId=u8681f7f7-ac3a-47bf-9bdf-359e94851e6&title=&width=703)
#### CMD中命令shell执行与exec执行区别
> [理解Docker容器的进程管理(PID1进程(容器内kill命令无法杀死)、进程信号处理、僵尸进程)](https://blog.csdn.net/m0_45406092/article/details/119007685)
> [Docker容器的init(1)进程](http://shareinto.github.io/2019/01/30/docker-init(1)/)

#### 12 个优化 Docker 镜像安全性的技巧

1. 避免泄露构建密钥
1. 以非 root 用户身份运行
1. 使用最新的基础镜像构建和更新系统包
1. 定期更新第三方依赖
1. 对你的镜像进行漏洞扫描
1. 扫描你的 Dockerfile 是否违反了最佳实践
1. 不要对 Docker Hub 使用 Docker 内容信任
1. 扫描你自己的代码是否有安全问题
1. 使用 docker-slim 来删除不必要的文件
1. 使用最小的基础镜像
1. 使用受信任的基础镜像
1. 测试你的镜像是否能在降低能力的情况下工作
> [12 个优化 Docker 镜像安全性的技巧](https://mp.weixin.qq.com/s/jTjQTEK9N4_P1YAp4miH7A)

## docker-compose
> [Docker Compose 详解](https://www.jianshu.com/p/658911a8cff3)

## Compose核心概念
一文件：docker-compose.yml
两要素：

- 服务（service）

一个个应用容器实例，比如订单微服务、库存微服务、mysql容器、nginx容器或者redis容器

- 工程（project）

由一组关联的应用容器组成的一个完整业务单元，在 docker-compose.yml 文件中定义。
## 使用Docker Compose编排SpringCloud微服务
使用docker-compose一次性来编排三个微服务:eureka服务(eureka-server-2.0.2.RELEASE.jar)、user服务(user-2.0.2.RELEASE.jar)、power服务(power-2.0.2.RELEASE.jar)
1.创建一个工作目录和docker-compose模板文件
2.工作目录下创建三个文件夹eureka、user、power，并分别构建好三个服务的镜像文件
以eureka的Dockerfile为例:
```dockerfile
# 基础镜像
FROM java:8
# 作者
MAINTAINER huaan
# 把可执行jar包复制到基础镜像的根目录下
ADD eureka-server-2.0.2.RELEASE.jar /eureka-server-2.0.2.RELEASE.jar
# 镜像要暴露的端口，如要使用端口，在执行docker run命令时使用-p生效
EXPOSE 8080
# 在镜像运行为容器后执行的命令
ENTRYPOINT ["java","-jar","/eureka-server-2.0.2.RELEASE.jar"]
```
目录文件结构：
```
compose
	docker-compose.yml
	eureka
		Dockerfile
		eureka-server-2.0.2.RELEASE.jar
	user
		Dockerfile
		user-2.0.2.RELEASE.jar
	power
		Dockerfile
		power-2.0.2.RELEASE.jar
```
3.编写docker-compose模板文件：
```yaml
version: '3.3'
services:
  eureka:
    image: eureka:v1
    ports:
     - 8080:8080
  user:
    image: user:v1
    ports:
     - 8081:8081
  power:
    image: power:v1
    ports:
     - 8082:8082
```
4.启动微服务，可以加上参数-d后台启动
docker-compose up -d
## 常用中间件docker搭建脚本
### apollo
```bash
docker run -p 8088:8088 \
    -e SERVER_PORT=8088 \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://113.31.144.178:33306/ApolloConfigDB?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=TIbQx46LRWZBeD93 \
    -d -v /root/docker/logs/apollo-configservice:/opt/logs --network host --name apollo-configservice apolloconfig/apollo-configservice

docker run -p 8090:8090 \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://113.31.144.178:33306/ApolloConfigDB?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=TIbQx46LRWZBeD93 \
    -d -v /root/docker/logs/apollo-adminservice:/opt/logs --network host --name apollo-adminservice apolloconfig/apollo-adminservice

docker run -p 8070:8070 \
    -e SPRING_DATASOURCE_URL="jdbc:mysql://113.31.144.178:33306/ApolloPortalDB?characterEncoding=utf8" \
    -e SPRING_DATASOURCE_USERNAME=root -e SPRING_DATASOURCE_PASSWORD=TIbQx46LRWZBeD93 \
    -e APOLLO_PORTAL_ENVS=dev \
    -e DEV_META=http://113.31.144.178:8088 \
    -d -v /root/docker/logs/ApolloPortal:/opt/logs --network host --name apollo-portal apolloconfig/apollo-portal
```
### mongodb
```bash
docker run --name mongo -v /root/docker/mongodb:/etc/mongo -v /root/docker/mongodb:/data/db -p 27117:27017  -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=n4oIzetBCvhsJyDa -d mongo
use points
db.createUser({user:'sa',pwd:'aqfyVDwsWKZjnrv2',roles:[{role:'readWrite',db:'points'},{role:'dbAdmin',db:'points'}]})
```
### mysql
```bash
docker run --name redis -p 6379:6379 -v /root/docker/redis:/data -d redis redis-server --appendonly yes --requirepass "9fpRaBGLnPbOXh8V"

```
> [https://www.cnblogs.com/ZMargo/category/1588902.html](https://www.cnblogs.com/ZMargo/category/1588902.html)