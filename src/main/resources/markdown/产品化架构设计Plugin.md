## 产品化架构设计  Plugin
作者：李明
时间：2022-07-07
## 一套代码，支持所有的差异化需求



## 一、为什么使用Plugin

以配置化支持客户对相同产品的差异性需求，实现了**以 No Code 的方式支持新客户的新产品**：  产品工厂！

![img](https://static001.geekbang.org/infoq/d2/d211a6558fd94c65b469c71810198add.png)



Plugin Framework，能够在无代码入侵的情况下，**实现产品基线的扩展点**（Extension Points），同时可以**让不同的客户需求以 Plugin 的形式接入**



需要注意的是，Plugin 容易被滥用；因此我们在架构上规定，尽可能用 Configuration 和 Composition 模式，仅在不得已的情况下使用 Plugin 模式，并且需求产品基线架构委员会进行需求和设计审核，才能在产品基线开出 Extension Point 扩展点。



![img](https://static001.geekbang.org/infoq/cd/cdc4a844310e5643bc4fcb872f5a1414.png)

https://www.processon.com/diagraming/62c443e60791293dccaec164



## 二、有哪些企业正在做这件事情

![众安保险](https://dm-pc.zacdn.cn/home/assets/images/newindex2021/zalogo2-high.svg)

由[蚂蚁金服](https://baike.baidu.com/item/蚂蚁金服/15897076)、[中国平安](https://baike.baidu.com/item/中国平安/7740464)和[腾讯](https://baike.baidu.com/item/腾讯/112204)于2013年联合发起设立。

**自研 Plugin Framework  进行客制化需求路线  2年+**



企业分享 ：

##### 保险产品 SaaS 化实践之路（上）

https://www.infoq.cn/article/BdUsDSvdyP3LYuLZ5Pzp

##### 保险产品 SaaS 化实践之路（下）

https://www.infoq.cn/article/QyWh5lTbPaGEj7zFoYGV





## 三、使用Plugin的优缺点 （项目化vs产品化）

![img](https://static001.geekbang.org/infoq/93/93a82b39c5c35309b272958c31ac946c.png)

**可协同，易管理**

![img](https://static001.geekbang.org/infoq/3e/3e5ddb764f78b39563c8394e57e268cc.png)





## 四、后续的一个应用前景

就目前兼达信息的 技术服务支持的发展路线来讲

项目 A  【网关、中间件A、聚合支付、定制化需求A、定制化需求B ....】

项目 B  【网关、中间件C、定制化需求A、定制化需求C ....】

项目 C 【网关、中间件A、聚合支付、定制化需求B ....】

使用Plugin的 方式进行差异化的需求开发，相同的模组可以在代码仓库进行管理，其他项目可以快速水平复制需要使用的服务/包，

#### 企业实操经验

**1、研发效率提升**。

**2、研发质量提升**。

**3、研发成本降低**。





## 五、演示  [插拔 - 客制化需求]

演示内容 ： 

1. 微服务运行期间的 **客制化插件**的 安装、停止、启动、卸载插件 ； 
2. 客制化接口的访问；

**Demo展示**

用户名 ：admin

密码 ： e10adc3949ba59abbe56e057f20f883e

客户端id ：admin_client

秘钥 ：f640e16a951b4109a88cf0fcd4d66a63

http://localhost:9000/doc.html#/home



## 六、演示  [插拔 - 工具/中间件]

演示内容 ： 

1. 自动配置插件路径，跟随微服务启动而运行使用；
2. 插入mybatis-plus插件（跟随微服务的数据源） 并访问；
3. 插入jpa插件（独立的数据源） 并访问；
4. 将老田写的 oss-starter 打包插入服务 (https://gitee.com/transcendall/kenvin-starter)

http://localhost:8080/doc.html#/home





## 七、构建一套Plugin Framework需要提供的生命周期方法

1. 卸载 
2. 安装
3. 状态检查
4. 启动
5. 停止



## 八、PF4J提供的扩展能力

**pf4j 相关链接 :**

pf4j开源git平台 : https://github.com/pf4j/pf4j

英文介绍 : https://javadoc.jenkins-ci.org/hudson/PluginWrapper.html

中文介绍 : https://www.5axxw.com/wiki/content/qf53n1#



## 九、Plugin工作原理

