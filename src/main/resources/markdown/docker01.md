## 什么是docker
> [什么是Docker](https://zhuanlan.zhihu.com/p/187505981)
> [Docker入门实践](http://c.biancheng.net/docker/)

![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654667358905-9e47022f-d6d9-4d53-9c7d-8f49d3422cd0.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=436&id=u991e21be&margin=%5Bobject%20Object%5D&name=image.png&originHeight=872&originWidth=1504&originalType=binary&ratio=1&rotation=0&showTitle=false&size=191673&status=done&style=none&taskId=ud291c6ab-ac9e-4ee7-b915-8a8bc816652&title=&width=752)
## Docker容器 VS VM
Docker容器和传统VM技术，在技术实现上有所不同。下图显示的是VM与Docker容器的逻辑组成：

- VM：使用Hypervisor提供虚拟机的运行平台，管理每个VM中操作系统的运行。每个VM都要有自己的操作系统、应用程序和必要的依赖文件等。
- Docker容器：使用Docker引擎进行调度和隔离，提高了资源利用率，在相同硬件能力下可以运行更多的容器实例；每个容器拥有自己的隔离化用户空间。


![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654667723710-b95d914a-5c68-4ba2-a1da-93114a440216.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=510&id=u689d5943&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1020&originWidth=1630&originalType=binary&ratio=1&rotation=0&showTitle=false&size=253918&status=done&style=none&taskId=u065ac821-dc40-4881-9d0f-b0227fb4f9b&title=&width=815)
Server：相当于云服务器
Host OS：为操作系统
Docker Engine：可以认为我们在这个操作系统上安装了一个docker的软件
App A：在Docker组件中运行App A
App B：在Docker组件中运行App B 
所以docker完全没有操作系统的概念，用的还是宿主机的操作系统，但是它做出了隔离。也实现了虚拟化。
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654667799912-8de693d6-8848-4a22-8671-332dbf2cc4da.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=196&id=u0c83ce13&margin=%5Bobject%20Object%5D&name=image.png&originHeight=391&originWidth=991&originalType=binary&ratio=1&rotation=0&showTitle=false&size=45700&status=done&style=none&taskId=u0af3f541-8fe1-4f84-841d-7a599b949f6&title=&width=495.5)
## docker镜像
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654660269222-066a37a9-d767-46bc-9046-7d2d23dea5c2.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=523&id=Yj9IW&margin=%5Bobject%20Object%5D&name=image.png&originHeight=1046&originWidth=1470&originalType=binary&ratio=1&rotation=0&showTitle=false&size=962326&status=done&style=none&taskId=u5abb56de-ed20-4815-a1c9-d7cb77fcc89&title=&width=735)
### 1.镜像是什么？
**镜像是一种轻量级、可执行的独立软件包，用来打包软件运行环境和基于运行环境开发的软件，它包含运行某个软件所需的所有内容，包括代码、运行时、库、环境变量和配置文件**
#### （1）Docker 镜像、容器的基石——联合文件系统（UnionFS）
UnionFS（联合文件系统）：Union文件系统是一种分层、轻量级并且高性能的文件系统，它支持对文件系统的修改作为一次提交来一层层的叠加，同时可以将不同目录挂载到同一个虚拟文件系统下，Union文件系统是Dokcer镜像的基础。镜像可以通过分层来进行继承，基于基础镜像（没有父镜像），可以制作各种具体的镜像。
特性：一次同时加载多个文件系统，但从外面看起来，只能看到一个文件系统，联合加载会把各层文件系统加载起来，这样最终的文件系统会包含所有的底层文件和目录
目前支持的联合文件系统种类包括AUFS、 btrfs、Device Mapper、 overlay 、 overlay2、vfs、zfs 等。多种文件系统目前的支持情况总结如下：
**AUFS**: 最早支持的文件系统，对于Debian/Ubuntu 支持好，虽然没有合并到Linux内核中，但成熟度很高
**btrfs**: 参考zfs等特性设计的文件系统，由Linux社区开发，视图未来取代Device Mapper,成熟度有待提高
**Device Mapper**: RedHat 公司和Docker 团队一起开发用于支持RHEL的文件系统，内核支持，性能略慢，成熟度高
**overlay**: 类似于AUFS的层次化文件系统，性能更好，从Linux 3.18 开始已经合并到内核，但成熟度有待提高
**overlay2**: Docker 1.12 后推出，原生支持128层，效率比OverlayFS高，较新的版本Dokcer支持
**vfs**: 基于普通文件系统（ext、nfs等）的中间层抽象，性能差，比较占用空间，成熟度也一般
**zfs**: 最初设计为Solarias 10 上的写时文件系统，拥有不少好的特性，但对于linux 支持还不够成熟
#### （2）Docker镜像加载原理
docker的镜像实际上是由一层一层的文件系统构成，这种层级的文件系统UnionFS。
主要包含bootloader和kernel，bootloader主要是引导加载kernel，Linux刚启动时会加载bootfs文件系统，在Docker镜像的最底层是bootfs。这一层与我们典型的linux/unix系统是一样的，包含boot加载器内核。当boot加载完之后整个内核就都在内存中了，此时内存的使用权已经由bootfs交给内核了，此时系统也会卸载bootfs
**平时我们安装进虚拟机的CentOS都是好几个G，为什么docker这里才200M**
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654660178586-161ac188-29f4-46f4-8ca9-ace53868730f.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=u37c39621&margin=%5Bobject%20Object%5D&name=image.png&originHeight=81&originWidth=709&originalType=url&ratio=1&rotation=0&showTitle=false&size=31916&status=done&style=none&taskId=ufdba817a-3d42-4ac9-ae51-c30888754aa&title=)
对以一个精简的OS，rootfs可以很小，只需要包括最基本的命令、工具和程序库就行，因为底层直接用host和kernel，自己只需要提供rootfs就行。由此可见对于不同的Linux发行版，bootfs基本是一致的，rootfs会有差别，因此不同的发行版可以公用bootfs。
#### （3）分层的镜像
以pull为例，在下载的过程中我么可以看到docker的镜像好像是在一层一层的下载

![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654660178591-28b28b82-313e-4cbb-a9c6-54dca9523dc7.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&id=udc688c3a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=346&originWidth=826&originalType=url&ratio=1&rotation=0&showTitle=false&size=131254&status=done&style=none&taskId=uf318abc8-6391-442a-9f7c-b0473284847&title=)
##### 假设Dockerfile 内容如下
```dockerfile
FROM ubuntu:14.04
ADD run.sh /
VOLUME /data
CMD ["./run.sh"]
```
##### 联合文件系统对应的层次结构如下图所示
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654660961270-c34c2c97-f115-4cd3-aedb-7af000cbc082.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=370&id=u2ba7071d&margin=%5Bobject%20Object%5D&name=image.png&originHeight=739&originWidth=1200&originalType=binary&ratio=1&rotation=0&showTitle=false&size=433177&status=done&style=none&taskId=u146d2eb8-d39f-46c3-8170-bf04ec25b26&title=&width=600)

- FROM ubuntu:14.04 :设置基础镜像，此时会使用基础镜像ubuntu:14.04的所* 有镜像层，为简单起见，图中将其作为一个整体展示。
- ADD run.sh /:将Dockerfile所在目录的文件run.sh加至镜像的根目录，此时新一层的镜像只有一项内容，即根目录下的run.sh.
- VOLUME /data:设定镜像的VOLUME，此VOLUME在容器内部的路径为/data。需要注意的是，此时并未在新一层的镜像中添加任何文件，但更新了镜像的json文件，以便通过此镜像启动容器时获取这方面的信息。
- CMD ["./run.sh"]:设置镜像的默认执行入口，此命令同样不会在新建镜像中添加任何文件，仅仅在上一层镜像json文件的基础上更新新建镜像的json文件。

图中的顶上两层，是Docker为Docker容器新建的内容，而这两层属于容器范畴。 这两层分别为Docker容器的初始层(Init Layer)与可读写层(Read－Write Layer)。

- 初始层: 大多是初始化容器环境时，与容器相关的环境信息，如容器主机名，主机host信息以及域名服务文件等。
- 读写层: Docker容器内的进程只对可读写层拥有写权限，其他层对进程而言都是只读的(Read-Only)。 另外，关于VOLUME以及容器的hosts、hostname、resolv.conf文件等都会挂载到这里。
#### （4）为什么Docker镜像要采用这种分层的结构
最大一个好处就是——共享资源
比如：有多个镜像都从相同的base镜像构建而来，那么宿主机只需要在磁盘上保存一份base镜像，同时内存中也需要加载一份base镜像，就可以为所有服务器服务了。而且镜像的每一层都可以被共享。
### 2.特点
docker镜像都是只读的
当容器启动时，一个新的可写层被加载到镜像的顶部。
这一层通常被称作“容器层”，“容器层”之下都叫“镜像层”
## 容器
Docker的运行组件，启动一个镜像就是一个容器，容器与容器之间相互隔离，并且互不影响。
## docker常见命令
### 镜像命令
#### docker info
#### docker --help
#### docker images  列出本机上的镜像
options:  -a :列出本地所有的镜像（含历史映像层） -q :只显示镜像ID。
#### docker search 从镜像仓库中搜索镜像
docker search [镜像]    
#### docker pull  从镜像仓库中拉取镜像
docker pull [镜像]  
#### docker system df 查看镜像/容器/数据卷所占的空间
#### docker rmi 从本地删除某个镜像
docker rmi  [镜像]
删除单个  docker rmi  -f 镜像ID
删除多个  docker rmi -f 镜像名1:TAG 镜像名2:TAG 
删除全部  docker rmi -f $(docker images -qa)
#### docker save 保存镜像到指定文件
docker save [OPTIONS] IMAGE [IMAGE...]
OPTIONS 说明：

- **-o :**输出到的文件。

docker save -o my_mysql.tar mysql
docker save > nginx.tar nginx:latest 
#### docker load  将导出的镜像文件加载入docker中
docker load [options]
OPTIONS 说明：

- **--input , -i : **指定导入的文件，代替 STDIN。
- **--quiet , -q : **精简输出信息。

docker load < nginx.tar 
docker load --input my_mysql.tar

### 容器命令
#### docker run 新建+启动容器
docker run [OPTIONS] **IMAGE **[COMMAND] [ARG...]
OPTIONS说明（常用）：有些是一个减号，有些是两个减号 
 --name="容器新名字"       为容器指定一个名称； 
-d: 后台运行容器并返回容器ID，也即启动守护式容器(后台运行)； 
-i：以交互模式运行容器，通常与 -t 同时使用； 
-t：为容器重新分配一个伪输入终端，通常与 -i 同时使用； 也即启动交互式容器(前台有伪终端，等待交互)； 
-P: 随机端口映射，大写P 
-p: 指定端口映射，小写p 
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654668307208-cc151a71-763a-4eed-a945-6e994856ef09.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=203&id=u54d312d9&margin=%5Bobject%20Object%5D&name=image.png&originHeight=406&originWidth=544&originalType=binary&ratio=1&rotation=0&showTitle=false&size=184146&status=done&style=none&taskId=u7c072722-0cf3-4a5b-8a9b-2573e5ee5a9&title=&width=272)
启动交互式容器(前台命令行):
使用镜像centos:latest以交互模式启动一个容器,在容器内执行/bin/bash命令。 
> docker run -it centos /bin/bash  
> 参数说明：
> -i: 交互式操作。 
> -t: 终端。 
> centos : centos 镜像。 
> /bin/bash：放在镜像名后的是命令，这里我们希望有个交互式 Shell，因此用的是 /bin/bash。 
> 要退出终端，直接输入 exit: 
> [#!/bin/bash和#!/bin/sh的区别_Linux](https://www.cnblogs.com/ziyue7575/p/eddd8990f7050f997fc74b73028530eb.html)

#### docker ps 列出当前容器
docker ps [OPTIONS] 
OPTIONS说明（常用）：  
-a :列出当前所有正在运行的容器+历史上运行过的 
-l :显示最近创建的容器。 
-n：显示最近n个创建的容器。 
-q :静默模式，只显示容器编号。 
#### 退出容器
有两种

1. exit     run进去容器，exit退出，容器停止
1. run进去容器，ctrl+p+q退出，容器不停止
#### docker start 启动当前停止的容器
docker start 容器ID或者容器名
#### docker restart 重启容器
docker restart 容器ID或者容器名
#### docker stop 停止容器
docker stop 容器ID或者容器名
#### docker kill 强制停止容器
docker kill 容器ID或容器名
#### docker rm 删除已停止的容器
docker rm 容器ID
一次性删除多个容器实例：
docker rm -f $(docker ps -a -q)
docker ps -a -q | xargs docker rm
#### 启动守护式容器(后台服务器)
在大部分的场景下，我们希望 docker 的服务是在后台运行的，
我们可以过 -d 指定容器的后台运行模式。
docker run -d 容器名
#### docker logs 查看容器日志
docker logs [OPTIONS] CONTAINER
OPTIONS说明：

- **-f : **跟踪日志输出
- **--since :**显示某个开始时间的所有日志
- **-t : **显示时间戳
- **--tail :**仅列出最新N条容器日志
#### docker top 查看容器内运行的细节
docker top 容器ID
#### docker inspect 查看容器内细节
docker inspect 容器ID
#### 进入正在运行的容器并以命令行交互
docker exec -it 容器ID bashShell
docker attach 容器ID
上述两个区别：
attach 直接进入容器启动命令的终端，不会启动新的进程；用exit退出，会导致容器的停止。
exec 是在容器中打开新的终端，并且可以启动新的进程；用exit退出，不会导致容器的停止。
**推荐使用 docker exec 命令，因为退出容器终端，不会导致容器的停止。**
#### 从容器内拷贝文件到主机上
容器→主机
docker cp  容器ID:容器内路径目的主机路径
#### 导入和导出容器
export 导出容器的内容留作为一个tar归档文件[对应import命令]
import 从tar包中的内容创建一个新的文件系统再导入为镜像[对应export]
案例：
docker export 容器ID > 文件名.tar
cat 文件名.tar | docker import - 镜像用户/镜像名:镜像版本号
docker  import  [options] file|url|  [repository[:tag]]
在使用export命令导出容器，import导入为镜像后，使用命令docker run -p 8812:8889 --name xxx -d 753  启动容器失败，报错如下：
docker: Error response from daemon: No command specified.
See 'docker run --help'.
错误提示缺少明确的命令；
解决方法： 
在导出容器的服务器使用docker ps --no-trunc命令查看具体的command，然后将command加入到启动命令最后即可。
 docker run -p 8812:8889 --name xxx -d 753 /bin/sh  -c 'java -Duser.timezone=Asia/Shanghai -cp /app/resources:/app/classes:/app/libs/* com.fp.epower.EpowerApplication'
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654688374732-a85c97d1-17cc-4305-a0a0-42eb60000322.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=78&id=uf772cf56&margin=%5Bobject%20Object%5D&name=image.png&originHeight=156&originWidth=2184&originalType=binary&ratio=1&rotation=0&showTitle=false&size=57241&status=done&style=none&taskId=uca48625c-5404-44fe-a1f8-534b4d1e2a0&title=&width=1092)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654688391716-c4ee825b-3ac9-4332-9be3-4fd2a3a84486.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=181&id=u4f5f5e7b&margin=%5Bobject%20Object%5D&name=image.png&originHeight=362&originWidth=1306&originalType=binary&ratio=1&rotation=0&showTitle=false&size=55271&status=done&style=none&taskId=u28caaffb-c153-4f8c-9c51-525ca521810&title=&width=653)
这里还需要进入导出的tar包里，加上相应环境变量
#### docker commit 提交容器副本使之成为一个新的镜像
docker commit -m="提交的描述信息" -a="作者" 容器ID 要创建的目标镜像名:[标签名]

![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654670938328-b8390d27-1255-4a66-922c-3e551c5b634c.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=478&id=uefd82430&margin=%5Bobject%20Object%5D&name=image.png&originHeight=956&originWidth=1350&originalType=binary&ratio=1&rotation=0&showTitle=false&size=662964&status=done&style=none&taskId=u915d7164-0ba3-4f47-8a04-ff68044d458&title=&width=675)
attach    Attach to a running container                 # 当前 shell 下 attach 连接指定运行镜像 
build     Build an image from a Dockerfile              # 通过 Dockerfile 定制镜像 
commit    Create a new image from a container changes   # 提交当前容器为新的镜像 
cp        Copy files/folders from the containers filesystem to the host path   #从容器中拷贝指定文件或者目录到宿主机中 
create    Create a new container                        # 创建一个新的容器，同 run，但不启动容器 
diff      Inspect changes on a container's filesystem   # 查看 docker 容器变化 
events    Get real time events from the server          # 从 docker 服务获取容器实时事件 
exec      Run a command in an existing container        # 在已存在的容器上运行命令 
export    Stream the contents of a container as a tar archive   # 导出容器的内容流作为一个 tar 归档文件[对应 import ] 
history   Show the history of an image                  # 展示一个镜像形成历史 
images    List images                                   # 列出系统当前镜像 
import    Create a new filesystem image from the contents of a tarball # 从tar包中的内容创建一个新的文件系统映像[对应export] 
info      Display system-wide information               # 显示系统相关信息 
inspect   Return low-level information on a container   # 查看容器详细信息 
kill      Kill a running container                      # kill 指定 docker 容器 
load      Load an image from a tar archive              # 从一个 tar 包中加载一个镜像[对应 save] 
login     Register or Login to the docker registry server    # 注册或者登陆一个 docker 源服务器 
logout    Log out from a Docker registry server          # 从当前 Docker registry 退出 
logs      Fetch the logs of a container                 # 输出当前容器日志信息 
port      Lookup the public-facing port which is NAT-ed to PRIVATE_PORT    # 查看映射端口对应的容器内部源端口 
pause     Pause all processes within a container        # 暂停容器 
ps        List containers                               # 列出容器列表 
pull      Pull an image or a repository from the docker registry server   # 从docker镜像源服务器拉取指定镜像或者库镜像 
push      Push an image or a repository to the docker registry server    # 推送指定镜像或者库镜像至docker源服务器 
restart   Restart a running container                   # 重启运行的容器 
rm        Remove one or more containers                 # 移除一个或者多个容器 
rmi       Remove one or more images       # 移除一个或多个镜像[无容器使用该镜像才可删除，否则需删除相关容器才可继续或 -f 强制删除] 
run       Run a command in a new container              # 创建一个新的容器并运行一个命令 
save      Save an image to a tar archive                # 保存一个镜像为一个 tar 包[对应 load] 
search    Search for an image on the Docker Hub         # 在 docker hub 中搜索镜像 
start     Start a stopped containers                    # 启动容器 
stop      Stop a running containers                     # 停止容器 
tag       Tag an image into a repository                # 给源中镜像打标签 
top       Lookup the running processes of a container   # 查看容器中运行的进程信息 
unpause   Unpause a paused container                    # 取消暂停容器 
version   Show the docker version information           # 查看 docker 版本号 
wait      Block until a container stops, then print its exit code   # 截取容器停止时的退出状态值 
### docker save和docker export的区别

- 对于Docker Save方法，会保存该镜像的所有历史记录
- 对于Docker Export 方法，不会保留历史记录，即没有commit历史
- docker save保存的是镜像（image），docker export保存的是容器（container）；
- docker load用来载入镜像包，docker import用来载入容器包，但两者都会恢复为镜像；
- docker load不能对载入的镜像重命名，而docker import可以为镜像指定新名称。
## 容器卷
> [Docker 容器数据卷(Data Volume)与数据管理](https://itbilu.com/linux/docker/4kiHC33_G.html)
> [https://blog.csdn.net/jerry_liufeng/article/details/119830189](https://blog.csdn.net/jerry_liufeng/article/details/119830189)

### 作用
将运用与运行的环境打包镜像，run后形成容器实例运行，但是我们对数据的要求希望是持久化的 Docker容器产生的数据，如果不备份，那么当容器实例删除后，容器内的数据自然也就没有了。 为了能保存数据在docker中我们使用卷。 
特点： 
1：数据卷可在容器之间共享或重用数据 
2：卷中的更改可以直接实时生效，爽 
3：数据卷中的更改不会包含在镜像的更新中 
4：数据卷的生命周期一直持续到没有容器使用它为止 
### Permission denied 
Docker挂载主机目录访问如果出现cannot open directory .: Permission denied 
解决办法：在挂载目录后多加一个--privileged=true参数即可 
如果是CentOS7安全模块会比之前系统版本加强，不安全的会先禁止，所以目录挂载的情况被默认为不安全的行为， 在SELinux里面挂载目录被禁止掉了额，如果要开启，我们一般使用--privileged=true命令，扩大容器的权限解决挂载目录没有权限的问题，也即 使用该参数，container内的root拥有真正的root权限，否则，container内的root只是外部的一个普通用户权限。 
### 案例
#### 宿主vs容器之间映射添加容器卷

1. 直接命令添加：

公式：docker run -it -v /宿主机目录:/容器内目录 ubuntu /bin/bash 

| docker run -it --name myu3 --privileged=true -v /tmp/myHostData:/tmp/myDockerData ubuntu /bin/bash  |
| --- |

2. 读写规则映射添加说明

读写(默认)：  docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:rw     镜像名
只读：容器实例内部被限制，只能读取不能写  docker run -it --privileged=true -v /宿主机绝对路径目录:/容器内目录:ro    镜像名
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654672050493-f68a7856-fc38-41e3-84ff-50337b89b806.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=206&id=u2de11a4c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=412&originWidth=1500&originalType=binary&ratio=1&rotation=0&showTitle=false&size=325620&status=done&style=none&taskId=uddea5ea2-ee37-4ce5-b827-fad9708a336&title=&width=750)
此时容器只能读，不能写；如果宿主机写入内容，可以同步给容器内，容器可以读取到。

3. 卷的继承和共享

容器1完成和宿主机的映射
docker run -it  --privileged=true -v /mydocker/u:/tmp --name u1 ubuntu 
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654672162013-31f0804f-0e74-4d01-8c75-162b8b34bad2.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=150&id=u09adf87a&margin=%5Bobject%20Object%5D&name=image.png&originHeight=300&originWidth=1454&originalType=binary&ratio=1&rotation=0&showTitle=false&size=219282&status=done&style=none&taskId=ubaea2ba4-2247-46f6-a264-3b37affeb73&title=&width=727)
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654672170873-110f43d0-7b13-4656-8563-432a3bebba33.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=112&id=u123f4382&margin=%5Bobject%20Object%5D&name=image.png&originHeight=224&originWidth=1066&originalType=binary&ratio=1&rotation=0&showTitle=false&size=116078&status=done&style=none&taskId=uf367c88f-95de-4e85-be63-13ac4f27277&title=&width=533)
容器2继承容器1的卷规则
docker run -it  --privileged=true --volumes-from父类  --name u2 ubuntu
![image.png](https://cdn.nlark.com/yuque/0/2022/png/331591/1654672198650-591d25a6-d613-4130-b13c-39520321b6d7.png#clientId=ub1a21eb6-6cb1-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=205&id=u304bc2c1&margin=%5Bobject%20Object%5D&name=image.png&originHeight=410&originWidth=1372&originalType=binary&ratio=1&rotation=0&showTitle=false&size=291945&status=done&style=none&taskId=uc1038798-8536-4f6a-92f3-14847f658b1&title=&width=686)
## docker 网络
> [Docker四种网络模式](https://www.jianshu.com/p/22a7032bb7bd)
> [docker网络模式](https://www.cnblogs.com/zuxing/articles/8780661.html)

### docker network 命令
docker network --help
Usage:  docker network COMMAND
Commands:

- connect     Connect a container to a network
- create      Create a network
- disconnect  Disconnect a container from a network
- inspect     Display detailed information on one or more networks
- ls          List networks
- prune       Remove all unused networks
- rm          Remove one or more networks

Run 'docker network COMMAND --help' for more information on a command.
```bash
# 配置容器的主机名
docker run --name t1 --network bridge -h [自定义主机名] -it --rm busybox
# 自定义DNS
docker run --name t1 --network bridge --dns 114.114 -it --rm busybox
# 给host文件添加一条
docker run --name t1 --network bridge --add-host [hostname]:[ip] -it --rm busybox
```
```bash
docker network create -d bridge --subnet "172.26.0.0/16" --gateway "172.26.0.1" mybr0
```
