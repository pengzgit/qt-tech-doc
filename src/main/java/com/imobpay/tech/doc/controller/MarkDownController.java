package com.imobpay.tech.doc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;

/**
 * @author margo
 * @date 2022/6/28
 */
@Controller
@RequestMapping("/md")
@Slf4j
public class MarkDownController {

    @RequestMapping("/read")
    @ResponseBody
    public ResponseEntity<String> read(String filename) {
        if(!StringUtils.hasText(filename)) {
            return new ResponseEntity<>("文件名必传", HttpStatus.BAD_REQUEST);
        }
        ClassPathResource classPathResource = new ClassPathResource("markdown/" + filename);
        StringBuilder sb = new StringBuilder();
        try(InputStream inputStream = classPathResource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));) {
            String line = null;
            while((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            return new ResponseEntity<>(sb.toString(), HttpStatus.OK);
        } catch (Exception e) {
            log.info("read md file error: {}", e.getMessage());
            log.error("read md file error: {}", e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
