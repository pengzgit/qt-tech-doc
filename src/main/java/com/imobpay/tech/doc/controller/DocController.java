package com.imobpay.tech.doc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author margo
 * @date 2022/6/28
 */
@Controller
@RequestMapping("/")
@Slf4j
public class DocController {

    @RequestMapping(value = {"/index", "index.html"})
    public String index() {
        return "index";
    }


    @RequestMapping("/post")
    public String post() {
        return "post";
    }

    @RequestMapping("/contact")
    public String contact(HttpServletRequest request) {

        return "contact";
    }


    @RequestMapping("/docker01")
    public String docker01() {
        return "container/docker01";
    }


    @RequestMapping("/docker02")
    public String docker02() {
        return "container/docker02";
    }


    @RequestMapping("/k8s01")
    public String k8s01() {
        return "container/k8s01";
    }

    @RequestMapping("/demo")
    public String demo() {
        return "demo";
    }

    @RequestMapping("/doc")
    public String doc(String filename, HttpServletRequest request) {
        request.setAttribute("filename", filename);
        return "doc";
    }

    @PostMapping("/sendMsg")
    public String sendMsg(String name, String email, String phone, String message, RedirectAttributes attributes) {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(name).append("\n")
                .append("email: ").append(email).append("\n")
                .append("phone: ").append(phone).append("\n")
                .append("message: ").append(message).append("\n");
        log.info("new message: \n {}", sb.toString());
        attributes.addFlashAttribute("info",1);
        return "redirect:/contact";
    }
}
