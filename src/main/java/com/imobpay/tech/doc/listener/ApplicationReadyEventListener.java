package com.imobpay.tech.doc.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;

/**
 * @author margo
 * @date 2022/6/28
 */
@Component
@Slf4j
public class ApplicationReadyEventListener implements ApplicationListener<ApplicationReadyEvent> {

    private ServletContext servletContext;

    @Autowired
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }


    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ConfigurableApplicationContext context = event.getApplicationContext();
        TomcatServletWebServerFactory contextBean = context.getBean(TomcatServletWebServerFactory.class);
        String path = contextBean.getContextPath();
        log.info("项目启动成功,path = {}", path);
        servletContext.setAttribute("path", "" + path);

    }
}
