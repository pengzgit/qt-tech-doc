package com.imobpay.tech.doc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechDocApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechDocApplication.class, args);
    }

}
